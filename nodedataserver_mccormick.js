//v.002

var http = require('http');
var fs = require('fs');
var path = require('path');

var foodPlaces = [
    {
      "value": "Chick-fil-A",
      "title": "Chick-fil-A",
      "options": [
         {
            "value": "01",
            "menuItem": "Grilled Chicken Sandwich on a Gluten-Free Bun"
         },
         {
            "value": "02",
            "menuItem": "Waffle Fries"
         }
      ]
   },
   {
      "value": "Chipotle",
      "title": "Chipotle",
      "options": [
         {
            "value": "01",
            "menuItem": "Burrito Bowl"
         },
         {
            "value": "02",
            "menuItem": "Corn Tortilla Tacos"
         }
      ]
   },
   {
      "value": "Denny's",
      "title": "Denny's",
      "options": [
         {
            "value": "01",
            "menuItem": "Hamburger without a Bun"
         },
         {
            "value": "02",
            "menuItem": "Nachos"
         },
         {
            "value": "03",
            "menuItem": "Mediterranean Grilled Chicken with Mashed Potatoes"
         }
      ]
   }
];
			


http.createServer(function (request, response) {
    console.log('request starting...');
    
    var url = request.url;
    
    var parsedURL = require('url').parse(url, true);
    console.log(parsedURL);
    console.log(parsedURL.query.query);
    
    var contentType = 'text/html';
    
    
    
    if(parsedURL.pathname === "/json")
    {	
		var command = parsedURL.query.command;
		
		var operand = parsedURL.query.operand;
		
		console.log("COMMAND: " + command + ", " + operand);

		
		if(command === "get")
		{
			
			response.writeHead(200, { 'Content-Type': contentType });
			    
			    //begin code changes
			    var menu = "";

                for(var index = 0; index < foodPlaces.length; index++)
                {
                    if(foodPlaces[index].value.indexOf(operand) != -1)
                    {
                        for(var index2 = 0; index2 < foodPlaces[index].options.length; index2++)
                        {
                            var menuItem = foodPlaces[index].options[index2].menuItem;
            
                            menu = menu.concat("", "<li>" + menuItem + "</li>"); 
                        }
                    }
                }
            console.log("RESULT: " + JSON.stringify(menu));
			response.end(menu, 'utf-8');
			    //end code changes
		}
		else
		{			  
			console.error('Error running query');
			response.writeHead(500);
			response.end();
			return;
		};

    	return;
    };    
    
    
    // Handle requests to serve files.
    
	var filePath = '.' + request.url;
	
	if (filePath == './') filePath = './index.html';
		
	var extname = path.extname(filePath);
	
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
		case '.html':
			contentType = 'text/html';
			break;
	}
	
	fs.exists(filePath, function(exists) {
	
		if(exists) {
			fs.readFile(filePath, function(error, content) {
				if(error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, { 'Content-Type': contentType });
					response.end(content, 'utf-8');
				}
			});
		}
		else {
			response.writeHead(404);
			response.end();
		}
	});
	
}).listen(8000);
console.log('Server running at http://127.0.0.1:8000/');
// http://localhost:8000/json?command=get&operand=age
// JSON.parse(jsonString);

